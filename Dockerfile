# Выбрал базовый образ 

FROM debian:10 as build

# Загрузка и установка утилит и либ для модулей 

RUN apt update && apt install -y wget gcc make libpcre3-dev libssl-dev zlib1g-dev

# Загрузка и установка nginx с указанными в конфиге модулями

RUN wget http://nginx.org/download/nginx-1.25.1.tar.gz && tar xvfz nginx-1.25.1.tar.gz && cd nginx-1.25.1 && ./configure --with-http_ssl_module --with-stream --with-mail && make && make install

#Запуск бинарника из втророго образа

FROM debian:10

# Указываю рабочую директорию

WORKDIR /usr/local/nginx/sbin

# Копирование из билда

COPY --from=build /usr/local/nginx/sbin/nginx .

# Создание папок для логов,конфигов и раскидка прав на папку

RUN mkdir ../logs ../conf && touch ../logs/error.log && chmod +x nginx

# Запуск nginx

CMD ["./nginx", "-g", "deamon off;"]
